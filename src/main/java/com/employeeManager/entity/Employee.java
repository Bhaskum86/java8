package com.employeeManager.entity;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="employee")
@Data
@NoArgsConstructor
public class Employee implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="EMP_ID")
	private Long id;
	
	@Column(name="NAME", nullable = false)
	private String name;
	@Column(name="DESIGNATION", nullable = false)
	private String designation;
	@Column(name="REPORTING_MANAGER", nullable = false)
	private String reportMngr;
	@Column(name="CONTACT_NUMBER", nullable = false)
	private Long contactNumber;
	@Column(name="EMAIL", nullable = false)
	private String email;
	@Column(name="IS_MANAGER", nullable = false)
	private Boolean isManager;
	@Column(name="JOINING_DATETIME", nullable = false)
	private  LocalDate joiningDate;
	@Column(name="WORK_EXP", nullable = false)
	private Integer workExperience;
}


