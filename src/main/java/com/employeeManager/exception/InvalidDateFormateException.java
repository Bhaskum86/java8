package com.employeeManager.exception;

public class InvalidDateFormateException extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public  InvalidDateFormateException(String userInput) {
		super("Invalid Date Format: "+userInput);
	}
}
