package com.employeeManager.exception;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.stereotype.Component;

import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@NoArgsConstructor
@Component
public class ErrorResponse {

	private String message;
    private LocalDateTime datetime;
    private List<String> details;
	
	public ErrorResponse(String msg,LocalDateTime dat, List<String> detail) {
        super();
        this.message = msg;
        this.details = detail;
        this.datetime=dat;
    }
 
    
    
}
