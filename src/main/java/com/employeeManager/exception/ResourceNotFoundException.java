package com.employeeManager.exception;

public class ResourceNotFoundException extends RuntimeException{

	
	public  ResourceNotFoundException(Long id) {
		super("Resource Not Found With Id: "+id);
	}
	
	public  ResourceNotFoundException() {
		super("Resource Not Found With");
	}
}
