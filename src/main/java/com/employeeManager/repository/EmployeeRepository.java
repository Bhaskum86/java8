package com.employeeManager.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.employeeManager.entity.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee ,Long>{

	List<Employee> findByjoiningDate(LocalDate date);

	//Optional<List<Employee>> findTop5ByJoiningDateOrderByIdDesc();

}
