package com.employeeManager.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.employeeManager.entity.Employee;

public interface EmployeeService {
	
	public Employee createEmployee(Employee emp);
	
	public List<Employee> getTop5EmployeeByJoiningDate();
	public List<Employee> getEmployeesByJoiningDateTime(LocalDate date);
	public Map<Boolean,List<Employee>> getEmployees();
	
	public List<Employee> getDNACandidateList();
//    public default List<String>   getNextWeekWorkingDat(){
//    	
//    	
//    }
    
}
