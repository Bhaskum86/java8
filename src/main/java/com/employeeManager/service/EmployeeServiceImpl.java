package com.employeeManager.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.employeeManager.entity.Employee;
import com.employeeManager.repository.EmployeeRepository;


@Service
public class EmployeeServiceImpl implements EmployeeService{

	@Autowired
	private EmployeeRepository empRepository;
	
	public Employee createEmployee(Employee emp) {
		return empRepository.save(emp);
		}
	
	public List<Employee> getTop5EmployeeByJoiningDate(){
		//return empRepository.findTop5ByJoiningDateOrderByIdDesc();
		
	  return	empRepository.findAll()
			  				 .stream()
		                     .sorted((o1,o2)->o1.getJoiningDate().compareTo(o2.getJoiningDate()))
		                     //.sort((o1,o2)->o2.getJoiningDate().compareTo(o1.getJoiningDate()))
		                     .limit(5)
		                     .collect(Collectors.toList());
	}
	
	
	public List<Employee> getEmployeesByJoiningDateTime(LocalDate date){
		return empRepository.findAll()
				             .stream()
				             .filter(v->v.getJoiningDate().isEqual(date))
				             .collect(Collectors.toList());
		//return empRepository.findByjoiningDate(date);
	}
	
	public Map<Boolean,List<Employee>> getEmployees(){
		return empRepository.findAll().stream().collect(Collectors.groupingBy(Employee::getIsManager));
	}
	public List<Employee> getDNACandidateList(){
		return empRepository.findAll()
				            .stream()
				            .filter((emp)->emp.getWorkExperience()>7)
				            .collect(Collectors.toList());
//		return empRepository.findAll()
//							.stream()
//							.filter((emp)->emp.getWorkExperience()>7)
//							.collect(Collectors.groupingByConcurrent(Employee::workExperience));
	}
	
	
}
