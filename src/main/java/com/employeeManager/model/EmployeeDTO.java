package com.employeeManager.model;

import java.time.LocalDate;

import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class EmployeeDTO {
	
	//@NotNull(message="Name required")
	@NotEmpty(message="Name required")
	private String name;
	
	@NotEmpty(message="Designation required")
	private String designation;
	
	@NotEmpty(message="Reporting Manager is required")
	private String reportMngr;
	
	@NotNull(message="Contact number required")
	@Min(value=10,message="Contact number must contian 10 digit")
	//@Max(value=12,message="Contact number should not contian  more than 12 digit")
	private Long  contactNumber;
	
	@NotEmpty(message="Email required")
	//@Email(message="Invalid Email")
	private String email;
	
	@NotNull(message="isManager is required")
	private Boolean isManager;
	@NotNull(message="Joining date is required")
	@FutureOrPresent(message="Joining date is invlaid")
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	//@JsonFormat(pattern = "MM/dd/yyyy")
	private  LocalDate joiningDate;
	@NotNull(message="Work Experience is required")
	@Min(value=1,message="Work Experience should be gretter than 1")
	private Integer workExperience;
}
