package com.employeeManager.controller;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("api/v0.1/utils")
public class PrintNumberBetweenRang {

	private static final Log LOG=LogFactory.getLog(EmployeeController.class);
	
	
	@GetMapping("/getRangOfNumber")
	public void getRangOfNumber(@RequestParam int start, 
			@RequestParam int end){
		IntStream.range(start,end+1).forEach(System.out::println);
//		List<Integer> numbList=IntStream.range(start,end+1).map(v->new Integer(v)).collect(Collectors.toList());
//		return new 	ResponseEntity<Employee>(numbList,HttpStatus.CREATED);	
	}
}
