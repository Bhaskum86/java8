package com.employeeManager.controller;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.employeeManager.entity.Employee;
import com.employeeManager.exception.InvalidDateFormateException;
import com.employeeManager.model.EmployeeDTO;
import com.employeeManager.service.EmployeeService;


@RestController
@CrossOrigin
@RequestMapping("api/v0.1/employee")
public class EmployeeController {
	
	@Autowired	
	private EmployeeService empService;	
	private static final Log LOG=LogFactory.getLog(EmployeeController.class);
	@Autowired
    private ModelMapper modelMapper;
	
	@PostMapping("/create")
	public ResponseEntity<Employee> addEmployee(@Valid @RequestBody EmployeeDTO emp){
		LOG.info("Method Started: "+ "addEmployee");
		LOG.info("User input: "+emp.toString());
		
		String localDate=emp.getJoiningDate().toString();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate localD=LocalDate.parse(localDate,formatter);
		LOG.info("After converting joining date format:"+localD);
		emp.setJoiningDate(localD);
		LOG.info("Employee Object:"+emp);
		
		return new 	ResponseEntity<Employee>(empService.createEmployee(modelMapper.map(emp, Employee.class)),HttpStatus.CREATED);	
	}
	
	@GetMapping("/getTop5EmpByJoininDate")
	public ResponseEntity<List<Employee>> getTop5EmployeeByJoiningDate(){
		LOG.info("Method Started: "+ "getAllEmployee");
//		List<Employee> listEmp=new ArrayList<>();
//		Optional<List<Employee>> listEmpOp=empService.getEmployeeByJoiningDate();
//		if(listEmpOp.isEmpty()) {
//			throw new ResourceNotFoundException();
//		}
//		listEmp=listEmpOp.get();
		return new 	ResponseEntity<List<Employee>>(empService.getTop5EmployeeByJoiningDate(),HttpStatus.OK);
	}
	
	@GetMapping("/getEmployeeByJoinigDateTime/{joininDate}")
	public ResponseEntity<List<Employee>> getEmployeeByJoiningDateTime(@PathVariable("joininDate")
	                                                String joininDate){
		//DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate localD=null;
		try {
				 localD=LocalDate.parse(joininDate,formatter);
		    }
		catch(Exception ex) 
		  {		
			 LOG.error("Parse Exception exist becasue invalid date formate passed"+ex.getLocalizedMessage());
			 throw new InvalidDateFormateException(ex.getLocalizedMessage());
		  }
		
		LOG.info("After pars string to LocalDateTime:"+	localD);
		
		return new ResponseEntity<List<Employee>>(empService.getEmployeesByJoiningDateTime(localD),
				HttpStatus.OK);
		
	}
	@GetMapping("/findAllEmployeeAndGroup")
	public Map<Boolean,List<Employee>> findAllEmployeeGroupByCategory(){
		
		//Map<String,List<String> > map=new HashMap<>();
		
		return empService.getEmployees();
		
	}

	@GetMapping("/findAllDNACandidates")
	public ResponseEntity<List<Employee>> getAllDNACandidates(){
	  return new ResponseEntity<List<Employee>>(empService.getDNACandidateList(), HttpStatus.OK);
	}

	
	
	
	

	

}
